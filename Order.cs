using System;

namespace Eshop
{
    class Order
    {
        public string id;
        public decimal price;
        public string address;
        public User user;
    }
}
