using System;

namespace group
{
    class Conflict
    {
        static void Main(string[] args)
        {
            SayHello("Asia!");
        }

        static void SayHello(string name)
        {
            Console.WriteLine("DzieńDobry " + name + " " + surname);
        }
    }
}
