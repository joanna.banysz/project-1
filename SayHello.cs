﻿using System;

namespace group
{
    class Conflict
    {
        static void Main(string[] args)
        {
            SayHello("Asia!");
        }

        static void SayHello(string name, string surname, string sex)
        {
            Console.WriteLine("DzieńDobry " + sex + " " + name + " " + surname);
        }
    }
}

